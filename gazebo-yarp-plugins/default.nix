{ stdenv, fetchFromGitHub, cmake, pkgconfig
, gazeboSimulator, ogre, ignition, tinyxml
, freeimage, tbb, yarp
}:

stdenv.mkDerivation rec {
  name = "gazebo-yarp-plugins-${version}";
  version = "0.1.2";

  src = fetchFromGitHub {
    owner = "robotology";
    repo = "gazebo-yarp-plugins";
    rev = "v0.1.2";
    sha256 = "1z6w276z9sszngw2hm05x0rrq0rkdmzz849ihcgymkiiq678n3z4";
  };

  cmakeFlags = [ "-DCMAKE_MODULE_PATH=${ogre}/lib/OGRE/cmake" ];

  buildInputs = [
    cmake
    pkgconfig
    gazeboSimulator.gazebo6
    gazeboSimulator.sdformat3
    ignition.math
    freeimage
    ogre
    tinyxml
    tbb
    yarp
  ];
  postInstall = ''
    echo $out
  '';
  meta = {
    description = "Gazebo plugin to interface GAZEBO with YARP";
    homepage = https://github.com/robotology/gazebo-yarp-plugins/;
    license = stdenv.lib.licenses.lgpl3;
    platforms = stdenv.lib.platforms.linux;
    maintainers = [ stdenv.lib.maintainers.nico202 ];
  };

  
}
