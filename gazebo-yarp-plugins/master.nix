{ stdenv, fetchFromGitHub
, cmake, yarp
}:

stdenv.mkDerivation rec {
  name = "gazebo-yarp-plugins-${version}";
  version = "11-04-2016";

  src = fetchFromGitHub {
    owner = "robotology";
    repo = "gazebo-yarp-plugins";
    rev = "f5af03fb324638d17e58a4fa8ea44e58b6b804f3";
    sha256 = "1lv90kf7k7s8dyg3srszb82cy4xn6qj65nn1wnajgl15rglg33m2";
  };

  buildInputs = [
    cmake yarp
  ];

  meta = {
    description = "Spiking neural network simulator";
    homepage = http://sourceforge.net/projects/nemosim/;
    license = stdenv.lib.licenses.gpl2;
    platforms = stdenv.lib.platforms.linux;
    maintainers = [ stdenv.lib.maintainers.nico202 ];
  };

  
}
