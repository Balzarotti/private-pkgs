{ stdenv, fetchFromGitHub
}:

stdenv.mkDerivation rec {
  name = "icub-gazebo-${version}";
  version = "2015-07-07";

  src = fetchFromGitHub {
    owner = "robotology-playground";
    repo = "icub-gazebo";
    rev = "gazebo6";
    sha256 = "0gfxrxmsahlrfkq62l26w25nzlcgxdrxmhv0p2m3mwlfrraw6742";
  };

#  buildInputs = [ ];

  installPhase = ''
      mkdir -p $out
      cp -r * $out
  '';

  meta = {
    description = "Simulation of iCub robot in Gazebo";
    homepage = https://github.com/robotology-playground/icub-gazebo;
    license = stdenv.lib.licenses.cc-by-30;
    platforms = stdenv.lib.platforms.linux;
    maintainers = [ stdenv.lib.maintainers.nico202 ];
  };
}
