{ stdenv, fetchFromGitHub, pkgconfig, boost155, cmake, cudatoolkit
, libtool, python
, enableMatlab ? false
, enablePython ? true
}:

stdenv.mkDerivation rec {
  name = "nemosim-${version}";
  version = "0.7.2";

  src = fetchFromGitHub {
    owner = "nico202";
    repo = "NeMosim";
    rev = "355119a30b6676bad2fb88cc675d1e94e2097533";
    sha256 = "17lc7gj6rfl5cyjqsr281p1mlb57q152kqkh3yhfqk0psw2z4bl5";
  };

#  patches = [ ./boost.patch ];

  buildInputs = [
    boost155 cmake cudatoolkit libtool python
  ];

  cmakeFlags = [
    "-DNEMO_CUDA_DEVICE_MAJOR:STRING=2"
    "-DNEMO_CUDA_DEVICE_MINOR:STRING=0"
    "-DNEMO_MATLAB_ENABLED:BOOL=OFF"
  ];

  postInstall = ''
    cd $out/share/nemo/python
    python setup.py install --prefix="$out"
  '';

  meta = {
    description = "Spiking neural network simulator";
    homepage = http://sourceforge.net/projects/nemosim/;
    license = stdenv.lib.licenses.gpl2;
    platforms = stdenv.lib.platforms.linux;
    maintainers = [ stdenv.lib.maintainers.nico202 ];
  };

  
}
